'use strict';

/*Сгенерировать массив из N случайных целых чисел. Поменять местами все элементы относительно середины массива по следующей схеме:
Было [1,2,3,4,5,6,7,8,9], стало [4,3,2,1,5,9,8,7,6]. Создавать новые массивы нельзя.*/

function swapArray(a) {
    if (a.length < 3) {
        [a[0], a[1]] = [a[1], a[0]];
        console.log("[" + a.toString() + "]");
        return;
    }
    if (a.length === 3) {
        [a[0], a[2]] = [a[2], a[0]];
        console.log("[" + a.toString() + "]");
        return;
    }

    let center = Math.floor((a.length) / 2);
    let odd = a.length % 2;
    let quarter = Math.floor(center / 2);

    for (let i = 0; i < quarter; i++) {
        // Левая часть
        let x = a[i];
        a[i] = a[center - 1 - i]; // Центр - 1(индекс начинается с 0) - смещаем на i. Для четного и нечетного формула одинакова, т.к. центр для них одинаков (для нечетного округляется)
        a[center - 1 - i] = x;

        //Правая часть
        x = a[i + center + odd]; // Начало = i + смещаем на центр + смещаем на 1, если нечетное количество. Т.к. центр для нечетного нужно пропустить
        a[i + center + odd] = a[a.length - 1 - i];
        a[a.length - 1 - i] = x;
    }
    console.log("[" + a.toString() + "]");
}


swapArray([1, 2, 3, 4, 5, 6, 7, 8, 9]);