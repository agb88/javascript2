'use strict';

/*6. Перевести пример на ООП мужчины и женщины на JavaScript 5. (edited)
Пример ООП мужчины и женщины имеет след. проблемы:
- Замужество смоделировано в одну сторону: Иван женат на Анне, но Анна не замужем за Иваном
- Дружба смоделирована в одну сторону: Петр - друг Ивана, но Иван не друг Петра.
Исправьте это.*/


function Person(name) {
    this.name = name;
    this.friends = [];
    this.marry = '';
}

Person.prototype.sayHello = function () {
    var text = "My name is " + this.name + ".";
    if (this.friends.length !== 0) {
        text += "\nMy Friends: ";
        for (var i = 0; i < this.friends.length; i++) {
            text += this.friends[i].name + ", ";
        }

    }
    if (this.marry !== '') {
        text += "\nMy husband/wife: " + this.marry;
    }
    console.log(text);
};

function Man(name) {
    Person.call(this, name);
}

function Woman(name) {
    Person.call(this, name);
}

var f = function(){};
f.prototype = Person.prototype;
Man.prototype = new f();
Man.prototype.constructor = Man;
Woman.prototype = new f();
Woman.prototype.constructor = Woman;

Man.prototype.setFriend = function (person) {
    if (person instanceof Man) {
        this.friends.push(person);
        person.friends.push(this);
    }
};

Man.prototype.getMarry = function (person) {
    if (person instanceof Woman && !this.marry) {
        this.marry = person.name;
        person.marry = this.name;
    }
};

Woman.prototype.setFriend = function (person) {
    if (person instanceof Woman) {
        this.friends.push(person);
        person.friends.push(this);
    }
};

Woman.prototype.getMarry = function (person) {
    if (person instanceof Man && !this.marry) {
        this.marry = person.name;
        person.marry = this.name;
    }
};



var ivan = new Man("Иван");
var anna = new Woman("Анна");
var petr = new Man("Петр");
ivan.setFriend(petr);
ivan.getMarry(anna);
ivan.sayHello();
anna.sayHello();
petr.sayHello();

