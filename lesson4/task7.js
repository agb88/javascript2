'use strict';

/*5. Пример ООП мужчины и женщины имеет след. проблемы:
- Замужество смоделировано в одну сторону: Иван женат на Анне, но Анна не замужем за Иваном
- Дружба смоделирована в одну сторону: Петр - друг Ивана, но Иван не друг Петра.
Исправьте это.*/

class Person {
    constructor(name) {
        this.name = name;
        this.friends = [];
        this.marry = '';
    }

    sayHello() {
        let text = "My name is " + this.name + ".";
        if (this.friends.length !== 0) {
            text += "\nMy Friends: ";
            for (let i = 0; i < this.friends.length; i++) {
                text += this.friends[i].name + ", ";
            }

        }
        if (this.marry !== '') {
            text += "\nMy husband/wife: " + this.marry;
        }
        console.log(text);
    }
}

class Man extends Person {
    constructor(name) {
        super(name);
    }

    setFriend(person) {
        if (person instanceof Man) {
            this.friends.push(person);
            person.friends.push(this);
        }
    }

    getMarry(person) {
        if (person instanceof Woman && !this.marry) {
            this.marry = person.name;
            person.marry = this.name;
        }
    }
}

class Woman extends Person {
    setFriend(person) {
        if (person instanceof Woman) {
            this.friends.push(person);
            person.friends.push(this);
        }
    }

    getMarry(person) {
        if (person instanceof Man && !this.marry) {
            this.marry = person.name;
            person.marry = this.name;
        }
    }
}

let ivan = new Man("Иван");
let anna = new Woman("Анна");
let petr = new Man("Петр");
ivan.setFriend(petr);
ivan.getMarry(anna);
ivan.sayHello();
anna.sayHello();
petr.sayHello();

