'use strict';

/*Домашнее задание B
Написать «чистую» («молчаливую») функцию для эффективного подсчёта количества русских гласных букв в строке.
Спросить у пользователя строку. Вывести в консоль количество русских гласных букв в ней.*/

var count;
const readline = require('readline'); // загружаем модуль
const rl = readline.createInterface({input: process.stdin, output: process.stdout});
rl.question("Введите строку: ", function (answer) {
    rl.close();
    if (!answer) {
        console.log('Ничего не введено');
    }
    else {
        count = countLetters(answer);
        console.log("Количество гласных: " + count);
    }
});

function countLetters(answer) {
    let count = 0;
    let arr = answer.split("");
    for (let i = 0; i < arr.length; i++) {
        switch (arr[i]){
            case "а":
            case "о":
            case "е":
            case "ё":
            case "и":
            case "э":
            case "ы":
            case "у":
            case "ю":
            case "я":
                count++;
                break;
        }
    }
    return count;
}


