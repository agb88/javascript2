'use strict';

const readline = require('readline'); // загружаем модуль

var data = {};


function askSurname() {
    const rl = prompt();
    rl.question("Ваше фамилия", (answer) => {
        rl.close();
        if (!answer) {
            console.log('Поле не может быть пустым.');
            askSurname("Ваше фамилия");
        }
        else {
            data["Фамилия"] = answer;
            askName();
        }
    });
}

function askName() {
    const rl = prompt();
    rl.question("Ваше имя", (answer) => {
        rl.close();
        if (!answer) {
            console.log('Поле не может быть пустым.');
            askName("Ваше имя");
        }
        else {
            data["Имя"] = answer;
            askPatronymic();
        }
    });
}

function askPatronymic() {
    const rl = prompt();
    rl.question("Ваше отчество", (answer) => {
        rl.close();
        if (!answer) {
            console.log('Поле не может быть пустым.');
            askPatronymic("Ваше отчество");
        }
        else {
            data["Отчество"] = answer;
            askAge();
        }
    });
}

function askAge() {
    const rl = prompt(); // создаем объект Интерфейс
    rl.question('Какой Ваш возраст? ', (answer) => { // запускаем prompt и callback (запускается функция, в которую передается полученный параметр)
        rl.close(); // закрываем поток
        const age = +answer; // приводим к числу возвраст
        if (isNaN(age) || !isFinite(age)) { // проверяем на число
            console.log('Возраст должен быть введен числом.');
            askAge(); // при неправильной вводе рекурсивно вызываем функцию
        }
        else {
            data["Возраст в годах"] = age; // пишем в объект
            data["Через 5 лет вам будет"] = +age + 5; // пишем в объект
            end(); // вызываем вывод в консоль
        }
    });
}

function prompt() {
    return readline.createInterface({input: process.stdin, output: process.stdout});
}

function end() {
    console.log(`ФИО: ${data["Имя"]} ${data["Отчество"]} ${data["Фамилия"]}, возраст ${data["Возраст в годах"]}, через 5 лет будет ${data["Через 5 лет вам будет"]}`);
}

askSurname();