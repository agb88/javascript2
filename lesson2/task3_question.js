'use strict';

const readline = require('readline'); // загружаем модуль

var surname = getString("Ваше фамилия");
var name = getString("Ваше имя");
var patronymic = getString("Ваше отчество");

end();

var data = {
    surname: surname,
    name: name,
    patronymic: patronymic
};

function getString(question) {
    const rl = prompt();
    var result = '';
    rl.question(question, (answer) => {
        console.log(answer);
        rl.close();
        if (!answer) {
            console.log('Поле не может быть пустым.');
            getString(question);
        }
        else {
            result = answer;
        }
    });
    return result;
}

function prompt() {
    return readline.createInterface({input: process.stdin, output: process.stdout});
}

function end() {
    console.log(`Вы ввели: ФИО ${data.surname} ${data.name} ${data.patronymic}`);
}