'use strict';

/*Домашнее задание D1
Переписать ДЗ B (подсчёт гласных букв в строке) без использования циклов, двумя способами:
с использованием метода массива forEach,
с использованием метода массива filter,
с использованием метода массива reduce.*/


const readline = require('readline'); // загружаем модуль
const rl = readline.createInterface({input: process.stdin, output: process.stdout});
rl.question("Введите строку: ", function (answer) {
    rl.close();
    if (!answer) {
        console.log('Ничего не введено');
    }
    else {
        console.log("Количество гласных forEach(): " + countLetters(answer));
        console.log("Количество гласных filter(): " + countLetters2(answer));
        console.log("Количество гласных reduce(): " + countLetters3(answer));
    }
});

function countLetters(answer) {
    let count=0;
    let etalon = {"а":true, "о":true, "е":true, "ё":true, "и":true, "э":true, "ы":true, "у":true, "ю":true, "я":true};
    let arr = answer.split("");
    arr.forEach((elem) => {
        if (elem.toLowerCase() in etalon)
        count++
    });
    return count;
}

function countLetters2(answer) {
    let etalon = {"а":true, "о":true, "е":true, "ё":true, "и":true, "э":true, "ы":true, "у":true, "ю":true, "я":true};
    let arr = answer.split("");
    let arr2 = arr.filter((elem) => {
            return elem.toLowerCase() in etalon;
    });
    return arr2.length;
}

function countLetters3(answer) {
    let etalon = {"а":true, "о":true, "е":true, "ё":true, "и":true, "э":true, "ы":true, "у":true, "ю":true, "я":true};
    let arr = answer.split("");
    return arr.reduce((count, elem) => {
        return elem.toLowerCase() in etalon?++count:count;
    }, 0);
}