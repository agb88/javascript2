'use strict';

/*Домашнее задание C2
Создать проект DRINKS.
1.
Разработать класс HashStorage (в файле HashStorage.js) для хранения в хэше произвольных пар ключ-значение.
Ключ может быть любой строкой; значение может иметь любой тип, в том числе векторный (хэш, массив и т.д.)
Класс должен иметь следующий интерфейс (т.е. иметь следующие публичные методы):
addValue(key,value) — сохраняет указанное значение под указанным ключом;
getValue(key) — возвращает значение по указанному ключу либо undefined;
deleteValue(key) — удаляет значение с указанным ключом, возвращает true если значение было удалено и false если такого значения не было в хранилище;
getKeys() — возвращает массив, состоящий из одних ключей.
Класс не должен использовать никаких глобальных переменных, не должен «пачкать экран».
Класс должен быть универсальным, т.е. не зависеть ни от структуры хранимых данных, ни от способа их последующего использования (в т.ч. не должен содержать никаких ссылок на DOM, т.к. может использоваться и вообще без веб-страницы).
2.
Создать объект drinkStorage класса HashStorage для хранения рецептов напитков.
Ключом является название напитка; значением — информация о напитке (алкогольный напиток или нет, строка с рецептом приготовления и т.д. по желанию).
3.
Разработать веб-страницу для работы с хранилищем рецептов напитков.
На странице должны быть кнопки:
«ввод информации о напитке» — последовательно спрашивает название напитка, алкогольный он или нет, рецепт его приготовления; сохраняет информацию о напитке в хранилище.
«получение информации о напитке» — спрашивает название напитка и выдаёт (на страницу, в консоль или в alert) либо информацию о нём (по примеру, приведённому ниже), либо сообщение об отсутствии такого напитка в хранилище.
«удаление информации о напитке» — спрашивает название напитка и удаляет его из хранилища (если он там есть) с выдачей соответствующего сообщения в информационное окно.
«перечень всех напитков» — выдаёт в информационное окно перечень всех напитков из хранилища (только названия).

Пример информации о напитке:

напиток Маргарита
алкогольный: да
рецепт приготовления:
продукт, продукт... смешать...*/

function HashStorage() {
    this.store = {};
    (function () {
        this.addValue = function (key, value) {
            this.store[key] = value;
        };

        this.getValue = function (key) {
            return this.store[key];
        };

        this.deleteValue = function (key) {
            if (this.store[key]){
                delete this.store[key];
                return true;
            }
            return false
        };

        this.getKeys = function () {
            return Object.keys(this.store);
        };
    }).call(HashStorage.prototype);
}

var drinkStorage = new HashStorage();

drinkStorage.addValue("Вода", {"Алкогольный": "нет", "Рецепт": "обыкновенная вода"});
drinkStorage.addValue("Вино", {"Алкогольный": "да", "Рецепт": "Виноград, сахар и т.д."});
drinkStorage.addValue("Водочка", {"Алкогольный": "да", "Рецепт": "C2H5OH"});


function add() {
    let name = prompt("Введите название", "Напиток 1");
    if (typeof name !== 'string') return;
    let alco = confirm("Алкогольный?");
    let recipe = prompt("Введите рецепт", "Рецепт 1");
    if (typeof recipe !== 'string') return;
    drinkStorage.addValue(name, {"Алкогольный": alco ? "да" : "нет", "Рецепт": recipe});
}

function get() {
    removeElements("p");
    let key = prompt("Введите название напитка", "Напиток 1");
    if (typeof key !== 'string') return;
    let drink = drinkStorage.getValue(key);
    let s = key + ":\n";
    for (let K in drink) {
        s += K + ": " + drink[K] + "\n";
    }
    let p = document.createElement("p");
    p.innerText = s;
    document.body.appendChild(p);
}

function deleteElement() {
    removeElements("p");

    let name = prompt("Введите название", "Напиток 1");
    if (typeof name !== 'string') return;
    (drinkStorage.deleteValue(name)) ? alert("Напиток удален") : alert("Напитка в базе нет");
}

function showAll() {
    removeElements("p");

    let keys = drinkStorage.getKeys();
    keys.forEach(function (item) {
        let p = document.createElement("p");
        p.innerText = item;
        document.body.appendChild(p);
    });
}

function removeElements(element) {
    let p = document.querySelectorAll(element);
    for (let i = 0; i < p.length; i++) {
        p[i].parentElement.removeChild(p[i]);
    }
}